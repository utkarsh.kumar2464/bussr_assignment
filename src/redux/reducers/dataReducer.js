import {
  USER_DATA,
  FILTERED_DATA,
  SEARCH_VALUE,
  LOADER,
  ADD_TITLE,
} from "../actions/type";

const initialState = {
  data: [],
  filteredData: [],
  value: "",
  loader: true,
  title: "",
};

export default listReducer = (state = initialState, action) => {
  const oldState = { ...state };
  const { type, payload } = action;
  switch (type) {
    case USER_DATA:
      return {
        ...oldState,
        data: payload,
      };
    case FILTERED_DATA:
      return {
        ...oldState,
        filteredData: payload,
      };
    case SEARCH_VALUE:
      return {
        ...oldState,
        value: payload,
      };
    case LOADER:
      return {
        ...oldState,
        loader: payload,
      };
    case ADD_TITLE:
      return {
        ...oldState,
        title: payload,
      };
  }
  return oldState;
};
