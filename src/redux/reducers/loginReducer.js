import { EMAILD_ID, PASSWORD_DATA } from "../actions/type";

const initialState = {
  emailid: "",
  password: "",
};

export default loginReducer = (state = initialState, action) => {
  const oldState = { ...state };
  const { type, payload } = action;
  switch (type) {
    case EMAILD_ID:
      return {
        ...oldState,
        emailid: payload,
      };
    case PASSWORD_DATA:
      return {
        ...oldState,
        password: payload,
      };
  }
  return oldState;
};
