import * as type from "./type";

export const userData = (payload) => ({
  type: type.USER_DATA,
  payload,
});

export const dataFiltered = (payload) => ({
  type: type.FILTERED_DATA,
  payload,
});

export const searchValue = (payload) => ({
  type: type.SEARCH_VALUE,
  payload,
});

export const emailID = (payload) => ({
  type: type.EMAILD_ID,
  payload,
});

export const passwordData = (payload) => ({
  type: type.PASSWORD_DATA,
  payload,
});

export const addData = (payload) => ({
  type: type.ADD_TITLE,
  payload,
});


export const loadingData = (payload) => ({
  type: type.LOADER,
  payload,
});
