import React, { useState, useEffect } from "react";
import { createNativeStackNavigator } from "../utils/modules";
import Container from "../containers/index";
import { ActivityIndicator } from "react-native";
import { auth } from "../utils/modules";
import SplashScreen from "../containers/SplashScreen/SplashScreen";
const Stack = createNativeStackNavigator();
const HomeStack = createNativeStackNavigator();

function HomeStackContainer() {
  return (
    <HomeStack.Navigator
      screenOptions={{
        headerShown: true,
      }}
    >
      <HomeStack.Screen
        name="HomeScreen"
        component={Container.HomeScreen}
        options={{
          headerShown: false,
        }}
      />

      <HomeStack.Screen
        name="AddData"
        component={Container.AddData}
        options={{ headerTitle: "Add Data to List" }}
      />
    </HomeStack.Navigator>
  );
}

const Navigator = () => {
  const [isLoading, setLoading] = useState(true);
  const [initRoute, setInitRoute] = useState(null);

  // Handle user state changes
  function onAuthStateChanged(user) {
    if (user) {
      setInitRoute("Home");
      setTimeout(() => {
        setLoading(false);
      }, 5000);
    } else {
      setInitRoute("Login");
      setTimeout(() => {
        setLoading(false);
      }, 5000);
    }
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (isLoading) {
    // We haven't finished checking for the token yet
    return <SplashScreen />;
  }

  return (
    <Stack.Navigator initialRouteName={initRoute}>
      <Stack.Screen
        name="Home"
        component={HomeStackContainer}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Login"
        component={Container.LoginScreen}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default Navigator;
