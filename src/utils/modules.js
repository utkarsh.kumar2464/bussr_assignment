import {
  NavigationContainer,
  useFocusEffect,
  CommonActions,
} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import auth from "@react-native-firebase/auth";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";

export {
  NavigationContainer,
  useFocusEffect,
  createNativeStackNavigator,
  auth,
  MaterialCommunityIcons,
  useDispatch,
  useSelector,
  CommonActions,
};
