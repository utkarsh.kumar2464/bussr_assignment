var ColorVariables = {
  background: "#fff",
  black:"#000000",
  primary: "#007bff",
  coolGrey: "#b0b6ba",
};

export { ColorVariables };
