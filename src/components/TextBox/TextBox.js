import React from "react";
import { TextInput, StyleSheet, Dimensions } from "react-native";
import { ColorVariables } from "../../utils/colorCode";
const { width } = Dimensions.get("window");

const TextBox = (props) => {
  return (
    <TextInput
      style={styles.input}
      onChangeText={props.onChangeText}
      value={props.value}
      placeholder={props.placeholder}
      placeholderTextColor={ColorVariables.coolGrey}
      secureTextEntry={props.secureTextEntry}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    height: 50,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: width - 40,
    borderColor: ColorVariables.coolGrey,
    borderRadius: 5,
    color: ColorVariables.black,
  },
});

export default TextBox;
