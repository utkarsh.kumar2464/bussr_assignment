import React from "react";
import { View, Text, Pressable, Dimensions } from "react-native";
import { ColorVariables } from "../../utils/colorCode";
const { width, height } = Dimensions.get("window");

const Button = (props) => {
  return (
    <Pressable
      style={{
        height: props.height || 50,
        width: props.width || width - 40,
        justifyContent: "center",
        borderRadius: props.radius || 5,
        backgroundColor: props.bgColor || ColorVariables.primary,
        elevation: 5,
        borderColor: props.bdColor,
        borderWidth: props.bdWidth,
      }}
      onPress={props.onclick}
    >
      <Text
        style={{
          textAlign: "center",
          color: props.textColor || ColorVariables.background,
          fontSize: props.size || 16,
          fontWeight: "700",
          fontFamily: "roboto",
        }}
      >
        {props.name}
      </Text>
    </Pressable>
  );
};

export default Button;
