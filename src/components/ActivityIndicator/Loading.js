import React from "react";
import { ActivityIndicator, StyleSheet } from "react-native";
import { ColorVariables } from "../../utils/colorCode";

const Loading = () => {
  return (
    <ActivityIndicator
      size={"large"}
      style={styles.indicator}
      color={ColorVariables.primary}
    />
  );
};

const styles = StyleSheet.create({
  indicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Loading;
