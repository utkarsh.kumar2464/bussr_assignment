import React from "react";
import {
  View,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { ColorVariables } from "../../utils/colorCode";
const { width, height } = Dimensions.get("window");
const SearchBox = (props) => {
  return (
    <View style={styles.searchBox}>
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        value={props.value}
        onChangeText={props.onChangeText}
        placeholder="Search"
        placeholderTextColor={ColorVariables.coolGrey}
        style={styles.input}
      />
      {props.value !== "" ? (
        <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}>
          <MaterialCommunityIcons
            name={"close"}
            size={26}
            style={{ paddingRight: 10 }}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  searchBox: {
    backgroundColor: "#fff",
    marginVertical: 20,
    borderRadius: 20,
    borderWidth: 2,
    borderRadius: 5,
    borderColor: ColorVariables.coolGrey,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  input: {
    backgroundColor: "#fff",
    paddingHorizontal: 20,
    width: width - 60,
    color: ColorVariables.black,
  },
});

export default SearchBox;
