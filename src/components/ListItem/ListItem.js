import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { ColorVariables } from "../../utils/colorCode";
import { MaterialCommunityIcons } from "../../utils/modules";

const ListItem = (props) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={props.onclick}
      style={styles.card}
    >
      <View style={styles.rowContainer}>
        <View>
          <Text style={styles.title}>{props.name}</Text>
          <Text style={styles.subtitle}>{props.uid}</Text>
        </View>
        {props.isChecked ? (
          <MaterialCommunityIcons
            name="checkbox-marked-circle"
            size={26}
            color={ColorVariables.primary}
          />
        ) : (
          <MaterialCommunityIcons
            name="checkbox-blank-circle-outline"
            size={26}
            color={ColorVariables.primary}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    borderWidth: 1,
    margin: 10,
    borderColor: ColorVariables.coolGrey,
    borderRadius: 5,
  },
  rowContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: "roboto",
  },
  subtitle: {
    fontSize: 12,
    fontWeight: "400",
    marginTop: 5,
    fontFamily: "roboto",
  },
});

export default ListItem;
