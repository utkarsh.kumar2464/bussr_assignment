import React, { useState } from "react";
import { View, Text, SafeAreaView, Image, ToastAndroid } from "react-native";
import { styles } from "./styles";
import { bussr } from "../../assets/index";
import { TextBox, Button } from "../../components/index";
import {
  auth,
  useSelector,
  useDispatch,
  CommonActions,
} from "../../utils/modules";
import { emailID, passwordData } from "../../redux/actions";
import { isEmailValid, isPasswordValid } from "../../helper/helper";

const LoginScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const { emailid, password } = useSelector(({ loginReducer }) => loginReducer);

  const signIn = () => {
    if (emailid && password) {
      if (isEmailValid(emailid) === true) {
        if (isPasswordValid(password) === true) {
          signintoAccont();
        } else {
          ToastAndroid.showWithGravity(
            "Password is not correct. Password must be of 8 Characters consist of letter and digit only",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
        }
      } else {
        ToastAndroid.showWithGravity(
          "Email is not correct",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
      }
    } else {
      ToastAndroid.showWithGravity(
        "Please enter the required fields",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
    }
  };

  const createAccount = () => {
    auth()
      .createUserWithEmailAndPassword(emailid, password)
      .then((res) => {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: "Home" }],
          })
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const signintoAccont = () => {
    auth()
      .signInWithEmailAndPassword(emailid, password)
      .then((res) => {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: "Home" }],
          })
        );
      })
      .catch((error) => {
        if (error.code === "auth/wrong-password") {
          ToastAndroid.showWithGravity(
            "Incorrect Password",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
        }
        if (error.code === "auth/user-not-found") {
          createAccount();
        }
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <Image source={bussr} />
      <Text style={styles.title}>Welcome to Bussr</Text>
      <View style={{ marginTop: 20 }}>
        <TextBox
          value={emailid}
          placeholder={"Enter Email Id *"}
          onChangeText={(text) => dispatch(emailID(text))}
        />
      </View>
      <View style={{ marginTop: 10 }}>
        <TextBox
          value={password}
          secureTextEntry={true}
          placeholder={"Enter Password *"}
          onChangeText={(text) => dispatch(passwordData(text))}
        />
      </View>
      <View style={{ marginTop: 20 }}>
        <Button name={"Sign In"} onclick={() => signIn()} />
      </View>
    </SafeAreaView>
  );
};

export default LoginScreen;
