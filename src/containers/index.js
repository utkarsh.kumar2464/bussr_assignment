import LoginScreen from "./LoginScreen/LoginScreen";
import HomeScreen from "./HomeScreen/HomeScreen";
import AddData from "./AddData/AddData";
import SplashScreen from "./SplashScreen/SplashScreen";
const Screens = {
  LoginScreen,
  HomeScreen,
  AddData,
  SplashScreen
};

export default Screens;
