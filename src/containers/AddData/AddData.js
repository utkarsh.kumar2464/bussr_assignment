import React from "react";
import { View, SafeAreaView, Keyboard, ToastAndroid } from "react-native";
import { Button, TextBox } from "../../components";
import { firebaseRefPush } from "../../helper/helper";
import { addData } from "../../redux/actions";
import { useDispatch, useSelector } from "../../utils/modules";
import { styles } from "./styles";

const AddData = ({ navigation }) => {
  const dispatch = useDispatch();
  const { title } = useSelector(({ dataReducer }) => dataReducer);

  return (
    <SafeAreaView style={styles.container}>
      <TextBox
        value={title}
        placeholder={"Enter Title *"}
        onChangeText={(text) => dispatch(addData(text))}
      />
      <View style={{ marginTop: 20 }}>
        <Button
          onclick={() => {
            if (title) {
              firebaseRefPush({ name: title });
              dispatch(addData(""));
              Keyboard.dismiss();
              navigation.goBack();
            } else {
              ToastAndroid.showWithGravity(
                "Please enter title",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
            }
          }}
          name={"Create"}
        />
      </View>
    </SafeAreaView>
  );
};

export default AddData;
