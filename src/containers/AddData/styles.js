import { StyleSheet } from "react-native";
import { ColorVariables } from "../../utils/colorCode";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems:"center",
    justifyContent:"center"
  },
 
});
