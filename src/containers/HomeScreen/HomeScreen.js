import React from "react";
import { FlatList, SafeAreaView, Text, View, Dimensions } from "react-native";
import { styles } from "./styles";
import { Button, ListItem, SearchBox, Loading } from "../../components";
import {
  firebaseRefRead,
  firebaseRefUpdate,
  firebaseRefWrite,
} from "../../helper/helper";
import {
  userData,
  dataFiltered,
  searchValue,
  loadingData,
} from "../../redux/actions";
import { useDispatch, useSelector, useFocusEffect } from "../../utils/modules";
const { width } = Dimensions.get("window");

const HomeScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const { data, filteredData, value, loader } = useSelector(
    ({ dataReducer }) => dataReducer
  );

  useFocusEffect(
    React.useCallback(async () => {
      let usersData = await firebaseRefRead();
      if (!usersData || usersData?.length <= 0) {
        usersData = await firebaseRefWrite(500);
      }

      dispatch(userData(usersData || []));
      dispatch(loadingData(false));

      return () => {
        handleSearch("");
      };
    }, [])
  );

  const getMoreData = async (limit) => {
    const usersData = await firebaseRefRead(limit);
    dispatch(userData(usersData || []));
  };

  const renderData = ({ item, index }) => {
    return (
      <ListItem
        name={item.name}
        uid={item.uid}
        isChecked={item.isChecked}
        onclick={() => onClick(item)}
      />
    );
  };

  const onClick = async (item) => {
    if (value) {
      const _fData = [...filteredData];
      const index = _fData.findIndex((a) => a.uid === item.uid);
      _fData[index] = { ..._fData[index], isChecked: !item.isChecked };
      dispatch(dataFiltered(_fData));
    }
    const _data = [...data];
    const index = data.findIndex((a) => a.uid === item.uid);
    _data[index] = { ..._data[index], isChecked: !item.isChecked };

    dispatch(userData(_data));
    await firebaseRefUpdate(item);
  };

  const handleSearch = (text) => {
    dispatch(searchValue(text));
    const formattedQuery = text.toLowerCase();
    let filter = data.filter((item) => {
      if (item?.name?.toLowerCase().match(formattedQuery)) {
        return item;
      }
    });
    dispatch(dataFiltered(filter));
  };

  const renderHeader = () => (
    <SearchBox
      onPress={() => dispatch(searchValue(""))}
      value={value}
      onChangeText={(text) => handleSearch(text)}
    />
  );

  if (loader) {
    return <Loading />;
  }
  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      <FlatList
        style={{ marginBottom: 50 }}
        data={!value ? data : filteredData}
        renderItem={renderData}
        keyExtractor={(item) => item.uid}
        onEndReachedThreshold={0.5}
        onEndReached={() => {
          if (!value && data?.length) getMoreData(data?.length + 100);
        }}
        ListEmptyComponent={() => (
          <View style={styles.emptyComponent}>
            <Text style={styles.emptyText}>No Data Found</Text>
          </View>
        )}
      />
      <View style={styles.bottomButton}>
        <Button
          name={"Add Data"}
          onclick={() => navigation.navigate("AddData")}
          width={width}
          radius={1}
        />
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
