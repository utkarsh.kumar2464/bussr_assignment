import { StyleSheet } from "react-native";
import { ColorVariables } from "../../utils/colorCode";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    marginTop: 20,
    fontFamily: "roboto",
  },
  emptyComponent: {
    justifyContent: "center",
    alignItems: "center",
  },
  emptyText: {
    color: ColorVariables.coolGrey,
    fontSize: 16,
    textAlign: "center",
    fontFamily: "roboto",
  },
  bottomButton: {
    position: "absolute",
    bottom: 0,
  },
});
