import React, { useRef, useEffect } from "react";
import { SafeAreaView, Image, Animated } from "react-native";
import { styles } from "../LoginScreen/styles";
import { bussr } from "../../assets/index";

const SplashScreen = () => {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 4000,
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Animated.View
        style={{
          opacity: fadeAnim,
        }}
      >
        <Image source={bussr} />
      </Animated.View>
    </SafeAreaView>
  );
};

export default SplashScreen;
