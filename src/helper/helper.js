import "react-native-get-random-values";

import { v4 as uuidv4 } from "uuid";
import database from "@react-native-firebase/database";
import { ToastAndroid } from "react-native";
const {
  uniqueNamesGenerator,
  adjectives,
  colors,
  animals,
} = require("unique-names-generator");

export const generatorRandomNames = (nameLength) => {
  const names = {};
  for (let i = 0; i < (nameLength || 10); i++) {
    const obj = { isChecked: false };
    const shortName = uniqueNamesGenerator({
      dictionaries: [adjectives, animals, colors],
      length: 2,
      separator: " ",
      style: "capital",
    });
    obj.name = shortName;
    obj.uid = uuidv4();
    obj.date = new Date().getTime();
    names[obj.uid] = obj;
  }
  return names;
};

export const firebaseRefRead = async (limit) => {
  await database().ref("/users");
  return await database()
    .ref("/users")
    .orderByChild("date", "desc")
    .limitToFirst(limit || 100)
    .once("value")
    .then((snap) => {
      const resultedData = [];
      snap.forEach((s) => resultedData.push(s.val()));
      return resultedData;
    });
};

export const firebaseRefWrite = async (length) => {
  await database().ref("/users");
  return await database()
    .ref("/users")
    .set(generatorRandomNames(length))
    .then(async () => await firebaseRefRead());
};

export const firebaseRefPush = async (_data) => {
  const uid = uuidv4();
  await database().ref(`/users`);
  return await database()
    .ref(`/users/${uid}`)
    .set({ uid, date: new Date().getTime(), isChecked: false, ..._data })
    .then(async () => {
      ToastAndroid.showWithGravity(
        "Data Added",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      return await firebaseRefRead();
    });
};

export const firebaseRefUpdate = async ({ uid, isChecked }) => {
  return await database()
    .ref(`/users/${uid}`)
    .update({
      isChecked: !isChecked,
    })
    .then(async () => {
      ToastAndroid.showWithGravity(
        "Data Updated",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      return await firebaseRefRead();
    })
    .catch(() => null);
};

export const isEmailValid = (email) => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return reg.test(email);
};

export const isPasswordValid = (password) => {
  let reg = /^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{7,}$/;
  return reg.test(password);
};
