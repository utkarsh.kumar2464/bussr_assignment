import React from "react";
import { NavigationContainer } from "./src/utils/modules";
import Navigator from "./src/routes";
import { Provider } from "react-redux";
import store from "./src/redux/store";

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Navigator />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
